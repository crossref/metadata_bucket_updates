from kafka import KafkaProducer
import boto3
import json
import zlib
import os
import time
import random
from kafka.admin import KafkaAdminClient, NewTopic

kafka_host = os.environ.get('KAFKA_HOST', 'localhost:9094')

topic_xml = os.environ.get('KAFKA_TOPIC_XML', 'metadata_s3_update_xml')
topic_citation = os.environ.get('KAFKA_TOPIC_CITATION', 'metadata_s3_update_citation')

topic_replication_factor = os.environ.get('REPLICATION_FACTOR', 1)
topic_partition_count = os.environ.get('PARTITION_COUNT', 1)
kafka_host = os.environ.get('KAFKA_HOST', 'localhost:9094')

if os.environ.get('CREATE_TOPICS'):
    admin_client = KafkaAdminClient(
        bootstrap_servers=kafka_host,
        client_id='metadata_bucket_updates.kafka_producer.lambda',
        api_version=(2,2,1),
        security_protocol='SSL'
    )

    topic_list = []
    topic_list.append(NewTopic(name=topic_xml, num_partitions=partition_count, replication_factor=replication_factor))
    topic_list.append(NewTopic(name=topic_citation, num_partitions=partition_count, replication_factor=replication_factor))
    admin_client.create_topics(new_topics=topic_list, validate_only=False)

s3 = boto3.client('s3')
producer = KafkaProducer(bootstrap_servers=kafka_host.split(','), api_version=(2,2,1), security_protocol='SSL')

def handler(event, context):

    for record in event['Records']:

        print('Handling record {}. '.format(record))

        start_time = time.time()
        bucket = record['s3']['bucket']['name']
        key = record['s3']['object']['key']

        try:
            message = {
                    's3_key': key,
                    's3_bucket': bucket
                    }
            message_json = json.dumps(message)

            topic = topic_xml
            if key.endswith('citation_update.json'):
                topic = topic_citation

            print('Sending {} to topic {}. '.format(message_json, topic))
            producer.send(topic, str.encode(message_json))
            producer.flush()
            print("=>   send usage time: {}s".format(time.time() - start_time))
            print("s3 key:{} push to kafka successful ".format(key))

            return key
        except Exception as e:
            raise e
