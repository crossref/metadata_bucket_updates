import argparse
import boto3
import os
import xml.etree.ElementTree as ET
import hashlib
from pathlib import Path

parser = argparse.ArgumentParser(description='Builds an S3 bucket of metadata using an extracted snapshot as the source. Note. Requires AWS_PROFILE environment variable to be set.')

parser.add_argument('--snapshot_path', type=str, dest='snapshot_path', required=True,
                   help='Path to the directory where the snapshot has been extracted.')

parser.add_argument('--destination_bucket', type=str, dest='destination_bucket', default='crossref-metadata-bucket-temp',
                   help='The name of the S3 bucket to build.')

parser.add_argument('--destination_directory', type=str, dest='destination_directory', default='',
                   help='The directory to output files to.')

args = parser.parse_args()
 
directory = args.snapshot_path
bucket = args.destination_bucket
destination_directory = args.destination_directory

if destination_directory == '':
    client = boto3.client('s3')

for filename in os.listdir(directory):
    try:
        it = ET.iterparse(os.path.join(directory, filename))
        for _, el in it:
            _, _, el.tag = el.tag.rpartition('}')
            if el.tag == 'crossref_metadata':
                el.tag = 'query'
            
        root = it.root

        for record in root.findall('.//record'):
            metadata = record.find('metadata')
            crossref_result = metadata.find('crossref_result')
            query_result = crossref_result.find('query_result')
            body = query_result.find('body')
            crossref_metadata = body.find('query')
            doi_text = crossref_metadata.find('doi').text
            doi = doi_text.encode('utf8').lower()
            sha_1 = hashlib.sha1()
            sha_1.update(doi)
            doi_hash = sha_1.hexdigest()

            citedby_count = crossref_metadata.find('crm-item[@name="citedby-count"]').text

            xml = ET.tostring(crossref_result, encoding='utf8', method='xml')
            json ='{"message": [["set", "'+doi_text+'", "is-cited-by-count", ' + citedby_count + ']]}'

            if destination_directory == '':
                print('Uploading', doi, 'with hash', doi_hash)
                client.put_object(
                        Bucket=bucket,
                        Body=xml,
                        Key='{hash}/{file}'.format(hash=doi_hash, file='unixsd.xml'),
                        Metadata={'cr-doi': doi_text, 'cr-source': filename}
                        )
                print('Uploading citation_update for', doi, 'with hash', doi_hash)
                client.put_object(
                        Bucket=bucket,
                        Body=json,
                        Key='{hash}/{file}'.format(hash=doi_hash, file='citation_update.json'),
                        Metadata={'cr-doi': doi_text, 'cr-source': filename}
                        )
            else:
                print('Copying', doi, 'with hash', doi_hash)
                Path(os.path.join(destination_directory, doi_hash)).mkdir(parents=True, exist_ok=True)
                with open(os.path.join(destination_directory, doi_hash, 'unixsd.xml'), 'wb+') as f:
                    f.write(xml)

                print('Copying citation_update for', doi, 'with hash', doi_hash)
                Path(os.path.join(destination_directory, doi_hash)).mkdir(parents=True, exist_ok=True)
                with open(os.path.join(destination_directory, doi_hash, 'citation_update.json'), 'wb+') as f:
                    f.write(json)

    except:
        print('Error processing file', filename)
        print(sys.exc_info())
