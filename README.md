# Metadata Bucket Updates

This repository contains the source used to:

1. Build an initial metadata bucket using a snapshot
2. Push incremental updates to the metadata bucket into Kafka

## Bucket Builder

Python was used here because it is a good choice to run an ad-hoc script like `build_bucket.py` and because it
has good support in AWS Lambda, which will be used for point 2 above.

You can run `bucket_builder/build_bucket.py` like so to get help:

```
python bucket_builder/build_bucket.py -h

usage: bucket_builder/build_bucket.py [-h] --snapshot_path SNAPSHOT_PATH [--destination_bucket DESTINATION_BUCKET]

Builds an S3 bucket of metadata using an extracted snapshot as the source. Note. Requires AWS_PROFILE environment variable to be set.

optional arguments:
  -h, --help            show this help message and exit
  --snapshot_path SNAPSHOT_PATH
                        Path to the directory where the snapshot has been extracted.
  --destination_bucket DESTINATION_BUCKET
                        The name of the S3 bucket to build.
  --destination_directory DESTINATION_DIRECTORY
                        The directory to output files to.
```

A command to build the bucket might look something like this:

```
 AWS_PROFILE=crossref-staging python bucket_builder/build_bucket.py --snapshot_path /mnt/BU1/snapshot/B --destination_bucket=crossref-metadata-bucket-temp
```

Or, if you don't want to build to S3, you can build to a local directory

```
 AWS_PROFILE=crossref-staging python bucket_builder/build_bucket.py --snapshot_path /mnt/BU1/snapshot/B --destination_directory /home/my-user/some-dir
```

**Note** The above requires that `AWS_PROFILE` `crossref-staging` exists.

The contents of the bucket are created in line with the spec defined [here](https://crossref.gitlab.io/knowledge_base/docs/services/metadata-bucket/#bucket-structure)  

## Kafka Pusher

Python was used here because it is a good choice for AWS Lambda, which is where this will be run.

This python function can be deployed as an AWS Lambda that will subscribe to changes in an S3 bucket, e.g. when new keys are added, it will propagate those new keys
as messages into a configured Kafka cluster. Keys in the bucket should be created in line with the spec [here](https://crossref.gitlab.io/knowledge_base/docs/services/metadata-bucket/#bucket-structure).

The following environment variables can be configured:

| Environment Variable | Default                    | Purpose                                                                                                      |
| -----------          | --------------             | ------------------------------------------------------------------------------------------------------------ |
| KAFKA_HOST           | localhost:9094             | Used to configure the kafka hosts to be used as bootstrap servers                                            |
| KAFKA_TOPIC_XML      | metadata_s3_update_xml      | The destination topic for xml metadata                                                                       |
| KAFKA_TOPIC_CITATION | metadata_s3_update_citation | The destination topic for json citation count data                                                           |
| REPLICATION_FACTOR   | 1                          | Used to configure the replication factor of KAFKA_TOPIC_XML and KAFKA_TOPIC_CITATION                         |
| PARTITION_COUNT      | 1                          | Used to configure the partition count of KAFKA_TOPIC_XML and KAFKA_TOPIC_CITATION                            |
| CREATE_TOPICS        |                            | When set to *any value* the code will try to create KAFKA_TOPIC_XML and KAFKA_TOPIC_CITATION before using them |
